const express = require('express')
const cors = require('cors');
require('./config/db');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

const { createUserValid, updateUserValid } = require('./middlewares/user.validation.middleware');
const { UserRepository } = require('./repositories/userRepository');

app.use('/', express.static('./client/build'));

// REST API USER
// app.get('/api/users', (req, res) => {
//     const users = JSON.stringify(UserRepository.getAll());
//     if(users){
//         res.status(200);
//     } else {
//         res.status(404);
//     }
//     res.send(users);
// });
// app.get('/api/users/:id', (req, res) => {
//     const user = JSON.stringify(UserRepository.getOne({id: req.params.id}));
//     console.log(user);
//     if(users){
//         res.status(200);
//     } else {
//         res.status(404);
//     }
//     res.send(user);
// });
// app.post('/api/users', (req, res) => {
//     createUserValid(req, res, UserRepository);
// });
// app.put('/api/users/:id', (req, res) => {
//     updateUserValid(req, res, UserRepository);
// });
// app.delete('/api/users/:id', (req, res) => {
//     const result = UserRepository.delete(req.params.id);
//     console.log("result");
//     console.log(result);
//     res.send(result);
// });


// REST API FIGHTER
app.get('/api/fighters', (req, res) => {
    res.send('Hello World, from express');
});
app.get('/api/fighters/:id', (req, res) => {
    res.send('Hello World, from express');
});
app.post('/api/fighters', (req, res) => {
    res.send('Hello World, from express');
});
app.put('/api/fighters/:id', (req, res) => {
    res.send('Hello World, from express');
});
app.delete('/api/fighters/:id', (req, res) => {
    res.send('Hello World, from express');
});

const port = 3050;
app.listen(port, () => {});

exports.app = app;