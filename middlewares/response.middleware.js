const responseMiddleware = (req, res, next) => {
   if(res.data){
    res.status(200).json(res.data);
   }else{
    res.status(400).json({
        error: true,
        message: res.err
    });
   }
    next();
}

exports.responseMiddleware = responseMiddleware;