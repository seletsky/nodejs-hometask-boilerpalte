const { user } = require('../models/user');
const UserService = require('../services/userService');
var uuid = require('uuid');

function validateUser(reqBody){
    let errorMessage = "";
    const validFirstName = /^[a-zA-Z ]{3,64}$$/.test(reqBody.firstName);
    const validLastName = /^[a-zA-Z ]{3,64}$$/.test(reqBody.lastName);
    const validEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(reqBody.email);
    const validPhone = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/.test(reqBody.phoneNumber);
    const validPassword = /^[a-zA-Z0-9]\w{8,14}$/.test(reqBody.password);
    const isAlreadyEmail = null;
    try {
        isAlreadyEmail = UserService.getOne({email: reqBody.email})
        errorMessage += validEmail ? "" : "Field  \"email\" is invalid value!\n";
    } catch (error) {
        // ignored 
    }
    errorMessage += isAlreadyEmail ? "Field  \"Email\" is already!\n" : "";
    errorMessage += validFirstName ? "" : "Field  \"firstName\" is invalid value!\n";
    errorMessage += validLastName ? "" : "Field  \"lastName\" is invalid value!\n";
    errorMessage += validPhone ? "" : "Field  \"phoneNumber\" is invalid value!\n";
    errorMessage += validPassword ? "" : "Field  \"password\" is invalid value!\n";
    return errorMessage;
}

const createUserValid = (req, res, next) => {
    const errorMessage = validateUser(req.body);
    if(errorMessage.length){
        throw Error(errorMessage);
    } else {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    const errorMessage = validateUser(req.body);
    if(errorMessage.length){
        throw Error(errorMessage);
    } else {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;