const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { fighter } = require('../models/fighter');

const router = Router();

router.get('/', (req, res) => {
    const fighters = FighterService.getAll();
    if(fighters){
        res.status(200);
    } else {
        res.status(404);
    }
    res.json(fighters);
})
.post('/', createFighterValid, (req, res) => {
    fighter.name = req.body.name;
    fighter.health = req.body.health;
    fighter.power = req.body.power;
    fighter.defense = req.body.defense;
    const result = FighterService.create(fighter);
    res.status(200);
    res.json(result);
});

router.get('/:id', (req, res) => {
    const fighter = JSON.stringify(FighterService.getOne({id: req.params.id}));
    if(fighter){
        res.status(200);
    } else {
        res.status(404);
    }
    res.json(fighter);
})
.put('/:id',updateFighterValid, (req, res) => {
    fighter.name = req.body.name;
    fighter.health = req.body.health;
    fighter.power = req.body.power;
    fighter.defense = req.body.defense;
    const result = FighterService.create(fighter);
    res.status(200);
    res.json(result);
})
.delete('/:id', (req, res) => {
    const result = FighterService.delete(req.params.id);
    if (result) {
        res.status(200);
        res.json(result);
    } else {
        res.status(400);
        res.json("User not found and not deleted!");
    }
});
module.exports = router;