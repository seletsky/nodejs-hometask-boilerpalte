const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { user } = require('../models/user');

const router = Router();

router.get('/', (req, res, next) => {
    try {
        res.data = UserService.getAll();
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)
.post('/', createUserValid, (req, res, next) => {
    try {
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;
        user.phoneNumber = req.body.phoneNumber;
        user.password = req.body.password;
        res.data = UserService.create(user);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        res.data = UserService.getOne({id: req.params.id});
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
},responseMiddleware)
.put('/:id',updateUserValid, (req, res,next) => {
    try {
        user.firstName = req.body.firstName;
        user.lastName = req.body.lastName;
        user.email = req.body.email;
        user.phoneNumber = req.body.phoneNumber;
        user.password = req.body.password;
        res.data = UserService.update(req.params.id, user);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware)
.delete('/:id', (req, res, next) => {
    try {
        res.data = UserService.delete(req.params.id);
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);
module.exports = router;