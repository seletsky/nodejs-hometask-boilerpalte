const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    _isNull(item) {
        if(!item) {
            return null;
        }else{
            return item;
        }
    }
    getAll(){
        const fighters = FighterRepository.getAll();
        return this._isNull(fighters);
    }

    getOne(search){
        const fighter = this.search(search);
        return this._isNull(fighter);
    }

    create(fighter){
        const item = FighterRepository.create(fighter);
        return this._isNull(item);
    }

    update(id, fighter){
        const item = FighterRepository.update(id, fighter);
        return this._isNull(item);
    }

    delete(id){
        const item = FighterRepository.delete(id);
        return this._isNull(item);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        return this._isNull(item);
    }
}

module.exports = new FighterService();