const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll(){
        const users = UserRepository.getAll();
        if(!users) {
            throw Error('Users not found');
        }
        return users;
    }

    getOne(search){
        const user = this.search(search);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }

    create(properties){
        const user = UserRepository.create(properties);
        if(!user) {
            throw Error('User not created');
        }
        return user;
    }

    update(id, properties){
        const user = UserRepository.update(id, properties);
        if(!user) {
            throw Error('User not updated');
        }
        return user;
    }

    delete(id){
        const user = UserRepository.delete(id);
        if(!user) {
            throw Error('User not deleted');
        }
        return user;
    }

    search(search) {
        const user = UserRepository.getOne(search);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new UserService();